<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description de ControleurUser
 *
 * @author thvincent6
 * @version 1.0
 *
 * C'est une classe qui permet de déléguer toutes les actions que peut faire un User de base.
 */
class ControleurUser {
    private $modeleAdmin;
    private $modele;

    /**
     * Permet d'instancier la classe avec tous les modèles utile
     *
     * @see ModeleAdmin::__construct
     * @see ModeleNews::__construct
     */
    public function __construct(){
        $this->modele = new ModeleNews();
        $this->modeleAdmin = new ModeleAdmin();
    }

    /**
     * Permet d'afficher les News en fonction d'une page et d'une limite de page donnée
     *
     * Fait un appel de la vue VueNews
     *
     * @see VueNews
     */
    public function afficherNews(){
        global $vues, $limitePage;
        $page = (isset($_GET['page'])) ? $page = $_GET['page'] : $page = 1;
        if(!Validation::valPAGE($page))
            throw new Exception();
        if(isset($_SESSION['taille'])){
            $taille=$_SESSION['taille'];
            $taille=Nettoyage::valInt($taille);
            $tabNews = $this->modele->getNews($page, $taille);
            $maxPage = ceil(($this->modele->getMaxPage())/$taille);
        }
        else{
            $tabNews = $this->modele->getNews($page, $limitePage);
            $maxPage = ceil(($this->modele->getMaxPage())/$limitePage);
        }
        require($vues['VueNews']);
    }

    /**
     * Permet de demander une connection pour devenir administrateur
     *
     * Fait un appel de la vue VueAuthentification
     *
     * @see VueAuthentification
     */
    public function demandeConnection(){
        global $vues;
        require($vues['VueAuthentification']);
    }

    /**
     * Permet de recupérer une demande de connection pour la valider ou non
     *
     * Fait des vérifications de ce qui provient du formulaire VueAuthentification
     * et fait de la délégation pour valider ou non.
     */
    public function connection(){
        global $vues;
        $login = $_POST['login'];
        $password = $_POST['mdp'];
        if(!empty($login) && !empty($password)){
            $user = $this->modeleAdmin->connection($login, $password);
            if(isset($user)){
                $message='Vous vous êtes connecté avec succès !';
                require($vues['VueSucces']);
            }
        }
        else{
            $message='Problème à la connection';
            require($vues['VueErreur']);
            require($vues['VueAuthentification']);
        }
    }
}
