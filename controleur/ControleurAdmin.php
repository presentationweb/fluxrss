<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description de ControleurAdmin
 *
 * @author thvincent6
 * @version 1.0
 *
 * C'est une classe qui permet de déléguer toutes les actions que peut faire un Administrateur.
 */
class ControleurAdmin {

    private $modele;
    private $modeleUser;

    /**
     * Permet d'instancier la classe avec tous les modèles utile
     *
     * @see ModeleAdmin::__construct
     * @see ModeleNews::__construct
     */
    public function __construct(string $action){
        $this->modele = new ModeleAdmin();
        $this->modeleUser = new ModeleNews();

        global $vues;
        try{
            switch ($action){
                // GESTION ADMIN :

                case 'deconnecter':
                    $this->deconnecter();
                    break;
                case 'parametrePage':
                    $this->parametrePage();
                    break;
                case 'setPage':
                    $this->setPage();
                    break;

                // GESTION DES NEWS :

                case 'afficherAjouterNews':
                    $this->afficherAjouterNews();
                    break;
                case 'ajouterNews':
                    $this->ajouterNews();
                    break;
                case 'afficherSupprimerNews':
                    $this->afficherSupprimerNews();
                    break;
                case 'supprimerNews':
                    $this->supprimerNews();
                    break;

                // GESTION DES FLUX :

                case 'afficherFlux':
                    $this->afficherFlux();
                    break;
                case 'afficherAjouterFlux':
                    $this->afficherAjouterFlux();
                    break;
                case 'ajouterFlux':
                    $this->ajouterFlux();
                    break;
                case 'afficherSupprimerFlux':
                    $this->afficherSupprimerFlux();
                    break;
                case 'supprimerFlux':
                    $this->supprimerFlux();
                    break;

                default:
                    $message='Vous utilisez une action qui est inconnue !';
                    require($vues['VueErreur']);
                    break;
            }
        }
        catch (Exception $e){

        }
    }

    /**
     * Permet de deconnecter un administrateur
     */
    public function deconnecter(){
        unset($_SESSION['login']);
        unset($_SESSION['role']);
        unset($_SESSION['taille']);
    }

    /**
     * Permet d'afficher la vue de paramètrage pour le nombre de News/Flux affiché
     */
    public function parametrePage(){
        global $vues;
        require($vues['VueTaillePage']);
    }

    /**
     * Permet d'affecter la valeur choisi
     *
     * @see parametrePage
     */
    public function setPage(){
        global $vues;
        if(isset($_SESSION['taille'])){
            if(isset($_POST['taille'])){
                $taille=$_POST['taille'];
                $taille=Nettoyage::valInt($taille);
                $_SESSION['taille']=$taille;
            }
            else{
                $message='Aucune valeur est recupérée !';
                require($vues['VueErreur']);
            }
        }
        else{
            $message='Tu as le droit de faire ça toi ?';
            require($vues['VueErreur']);
        }
    }

    /**
     * Permet d'afficher la vue pour ajouter une News
     */
    public function afficherAjouterNews(){
        global $vues;
        require($vues['VueAjouterNews']);
    }

    /**
     * Permet de lancer la délégation pour ajouter une News en base de donnée.
     *
     * Elle vérifie toutes les données puis délégue.
     */
    public function ajouterNews(){
        global $vues;
        if(isset($_POST['url']) && isset($_POST['heure']) && isset($_POST['ref']) && isset($_POST['titre']) && isset($_POST['desc'])){
            $url = $_POST['url'];
            $heure = $_POST['heure'];
            $ref = $_POST['ref'];
            $titre = $_POST['titre'];
            $desc = $_POST['desc'];

            if(!Validation::valURL($url)){
                Nettoyage::valUrl($url);
            }
            if(!Validation::valHEURE($heure)){

            }
            Nettoyage::valText($ref);
            Nettoyage::valText($titre);
            Nettoyage::valText($desc);
        }

        $this->modele->ajouterNews(new News($url, $heure, $ref, $titre, $desc));

        $message='La News est ajoutée avec succès !';
        require($vues['VueSucces']);
    }

    /**
     * Permet d'afficher la vue pour supprimer des News
     */
    public function  afficherSupprimerNews(){
        global $vues, $limitePage;
        $page = (isset($_GET['page'])) ? $page = $_GET['page'] : $page = 1;
        if(!Validation::valPAGE($page))
            throw new Exception();
        if(isset($_SESSION['taille'])){
            $taille=$_SESSION['taille'];
            $taille=Nettoyage::valInt($taille);
            $tabNews = $this->modeleUser->getNews($page, $taille);
            $maxPage = ceil(($this->modeleUser->getMaxPage())/$taille);
        }
        else{
            $tabNews = $this->modeleUser->getNews($page, $limitePage);
            $maxPage = ceil(($this->modeleUser->getMaxPage())/$limitePage);
        }
        require($vues['VueSupprimerNews']);
    }

    /**
     * Permet de lancer la délégation pour supprimer une News en base de donnée.
     *
     * Elle vérifie toutes les données puis délégue.
     */
    public function supprimerNews(){
        global $vues;
        $url = $_GET['id'];
        if(isset($url)){
            Nettoyage::valUrl($url);
        }
        $this->modele->supprimerNews($url);
        $message='La news a était supprimée avec succès !';
        require($vues['VueSucces']);
    }

    /**
     * Permet d'afficher les Flux en fonction d'une page et d'une limite de page donnée
     *
     * Fait un appel de la vue VueFlux
     *
     * @see VueFlux
     */
    public function afficherFlux(){
        global $vues, $limitePage;
        $page = (isset($_GET['page'])) ? $page = $_GET['page'] : $page = 1;
        if(!Validation::valPAGE($page))
            throw new Exception();
        if(isset($_SESSION['taille'])){
            $taille=$_SESSION['taille'];
            $taille=Nettoyage::valInt($taille);
            $tabFlux = $this->modele->getFlux($page, $taille);
            $maxPage = ceil(($this->modele->getMaxPageFlux())/$taille);
        }
        else{
            $tabFlux = $this->modele->getFlux($page, $limitePage);
            $maxPage = ceil(($this->modele->getMaxPageFlux())/$limitePage);
        }
        require($vues['VueFlux']);
    }


    /**
     * Permet d'afficher la vue pour ajouter un Flux
     */
    public function afficherAjouterFlux(){
        global $vues;
        require($vues['VueAjouterFlux']);
    }

    /**
     * Permet de lancer la délégation pour ajouter un Flux en base de donnée.
     *
     * Elle vérifie toutes les données puis délégue.
     */
    public function ajouterFlux(){
        global $vues;
        if(isset($_POST['url']) && isset($_POST['titre']) && isset($_POST['desc'])){
            $url = $_POST['url'];
            $titre = $_POST['titre'];
            $desc = $_POST['desc'];

            Nettoyage::valUrl($url);
            Nettoyage::valText($titre);
            Nettoyage::valText($desc);
        }

        $this->modele->ajouterFlux(new Flux($url, $titre, $desc));

        $message='Le Flux est ajoutée avec succès !';
        require($vues['VueSucces']);
    }

    /**
     * Permet d'afficher la vue pour supprimer des Flux
     */
    public function afficherSupprimerFlux(){
        global $vues, $limitePage;
        $page = (isset($_GET['page'])) ? $page = $_GET['page'] : $page = 1;
        if(!Validation::valPAGE($page))
            throw new Exception();
        if(isset($_SESSION['taille'])){
            $taille=$_SESSION['taille'];
            $taille=Nettoyage::valInt($taille);
            $tabFlux = $this->modele->getFlux($page, $taille);
            $maxPage = ceil(($this->modele->getMaxPageFlux())/$taille);
        }
        else{
            $tabFlux = $this->modele->getFlux($page, $limitePage);
            $maxPage = ceil(($this->modele->getMaxPageFlux())/$limitePage);
        }
        require($vues['VueSupprimerFlux']);
    }

    /**
     * Permet de lancer la délégation pour supprimer un Flux en base de donnée.
     *
     * Elle vérifie toutes les données puis délégue.
     */
    public function supprimerFlux(){
        global $vues;
        $url = $_GET['id'];
        if(isset($url)){
            Nettoyage::valUrl($url);
        }
        $this->modele->supprimerFlux($url);
        $message='Le flux a était supprimé avec succès !';
        require($vues['VueSucces']);
    }
}