<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description de ControleurPrincipal
 *
 * @author thvincent6
 * @version 1.0
 *
 * C'est une classe qui permet de déléguer toutes les actions que peut fait un utilisateur.
 */
class ControleurPrincipal {

    /**
     * Permet de faire la délégation depuis une action passée dans l'url.
     *
     * Si un utilisateur souhaite faire des actions d'administrateur, il decide
     * de lancer une page d'authentification pour pouvoir faire cette action.
     *
     * @see ControleurUser::__construct
     * @see ControleurAdmin::__construct
     */
    public function __construct() {
        global $vues;
        $controleurUser = new ControleurUser();
        $listeAction_Admin = array('deconnecter', 'parametrePage', 'setPage', 'afficherAjouterNews', 'ajouterNews', 'afficherSupprimerNews', 'supprimerNews', 'afficherFlux', 'afficherAjouterFlux', 'ajouterFlux', 'afficherSupprimerFlux', 'supprimerFlux');
        try{
            $admin = ModeleAdmin::isAdmin();
            $action = $_REQUEST['action'];
            if(in_array($action, $listeAction_Admin)){
                if(!isset($admin)){
                    require($vues['VueAuthentification']);
                }
                else{
                    new ControleurAdmin($action);
                }
            }
            else{
                switch ($action) {
                    case NULL:
                        $controleurUser->afficherNews();
                        break;
                    case 'connection':
                        $controleurUser->connection();
                        break;
                    case 'demandeConnection':
                        $controleurUser->demandeConnection();
                        break;
                    default:
                        $message='Vous utilisez une action qui est inconnue !';
                        require($vues['VueErreur']);
                        break;
                }
            }
        }
        catch (Exception $e){ echo $e->getMessage();
     
        }
    }
}
