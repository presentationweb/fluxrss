# FluxRSS en php

## Contexte

Pendant mon cours de php j'ai réalisé un site php qui avait pour but de lire des fluxs *RSS et les ajouter en base de donnée à l'aide d'un parseur.
    
## Technique

- HTML
- php
- SQL
- phpMyAdmin
- Bootstrap

## Information pratique

Pour faire fonctionner le site il faut importer la base de donnée nommée par défault dbthvincent
Si besoin il faut modifier le nom dans le fichier Config.php tout comme le user et password

Pour le site, pour utiliser la fonctionnalité admin est :
- L'utilisateur est : admin
- Le mot de passe est : admin