<?php
/**
 * Created by PhpStorm.
 * User: thvincent6
 * Date: 09/01/20
 * Time: 15:46
 */

/**
 * Description de Flux
 *
 * @author thvincent6
 * @version 1.0
 *
 * Un Flux est composée d'une url, titre, et une desc.
 * Il est constitué de ses Getter et Setter sans restriction.
 */
class Flux{
    private $url;
    private $titre;
    private $description;

    public function __construct($url, $titre, $description){
        $this->url = $url;
        $this->titre = $titre;
        $this->description = $description;
    }

    public function getUrl(){
        return $this->url;
    }

    public function getTitre(){
        return $this->titre;
    }

    public function getDescription(){
        return $this->description;
    }

    public function setUrl($url): void{
        $this->url = $url;
    }

    public function setTitre($titre): void{
        $this->titre = $titre;
    }

    public function setDescription($description): void{
        $this->description = $description;
    }

    /**
     * Réécriture d'un Flux sous le format :
     *
     * <Titre> \n <Description>
     *
     * @return Un string du Flux
     */
    public function __toString(): string {
        return $this->getTitre() . '<br />'. $this->getDescription() .'<br />';
    }
}
