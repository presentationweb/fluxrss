<?php
/**
 * Created by PhpStorm.
 * User: thvincent6
 * Date: 09/01/20
 * Time: 15:59
 */

/**
 * Description de FluxGateway
 *
 * @author thvincent6
 * @version 1.0
 *
 * C'est une classe qui permet de faire la pacerelle entre les Flux et la base de donnée.
 */
class FluxGateway{
    private $db;

    /**
     * Permet d'instancier la classe avec une Connection
     *
     * @param db C'est une instance de Connection qui permet de l'instancier une seule fois
     */
    public function __construct(Connection $db) {
        $this->db=$db;
    }

    /**
     * Permet de chercher en base de donnée tous les Flux avec une taille et une page donnée.
     *
     * @param page C'est le numéro de la page à afficher
     * @param taille C'est le nombre de Flux à afficher
     *
     * @return La liste des Flux donnée en fonction des paramètres
     */
    public function getFlux(int $page, int $taille){
        $query = 'SELECT * FROM Flux LIMIT :limitePage OFFSET :page';
        $this->db->executeQuery($query , array(
            ':limitePage' => array($taille, PDO::PARAM_INT),
            ':page' => array(($page-1)*$taille, PDO::PARAM_INT)
        ));
        return $this->db->getResults();
    }

    /**
     * Permet de savoir le nombre de page maximal à afficher.
     *
     * @return Un entier qui est le nombre de page maximal.
     */
    public function getMaxPageFlux() :int{
        $query = 'SELECT * FROM Flux';
        $this->db->executeQuery($query, array());
        $result = $this->db->getResults();
        return count($result);
    }

    /**
     * Permet de rajouter un Flux en base de donnée
     *
     * @param flux C'est le Flux qui va être ajouté en base de donnée
     */
    public function insert(Flux $flux) {
        $query = 'INSERT INTO Flux VALUES(:url, :titre, :desc)';
        $this->db->executeQuery($query, array(
            ':url' => array($flux->getUrl(), PDO::PARAM_STR),
            ':titre' => array($flux->getTitre(), PDO::PARAM_STR),
            ':desc' => array($flux->getDescription(), PDO::PARAM_STR)
        ));
    }

    /**
     * Permet de modifier un Flux en base de donnée (A coder)
     *
     * @param flux C'est le Flux à modifier
     */
    public function update(Flux $flux) {

    }

    /**
     * Permet de supprimer un Flux en base de donnée
     *
     * @param url C'est la clé primaire d'un Flux
     */
    public function delete(string $url) {
        $query = 'DELETE FROM Flux WHERE URL = :url';
        $this->db->executeQuery($query , array(
            ':url' => array($url, PDO::PARAM_STR)
        ));
    }
}
