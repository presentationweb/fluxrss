<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description de AdminGateway
 *
 * @author thvincent6
 * @version 1.0
 *
 * C'est une classe qui permet de faire la pacerelle entre les Admin et la base de donnée.
 */
class AdminGateway {
    private $db;

    /**
     * Permet d'instancier la classe avec une Connection
     *
     * @param db C'est une instance de Connection qui permet de l'instancier une seule fois
     */
    public function __construct(Connection $db) {
        $this->db=$db;
    }

    /**
     * Permet de chercher en base de donnée si un login existe.
     *
     * @param login Login à chercher en base
     *
     * @return La liste des Admin donnée si il existe (normalement renvoie que une taille de liste entre 0 et 1)
     */
    public function verif_mdp(string $login) : array {
        $query = 'SELECT * FROM admin WHERE login = :login';

        $this->db->executeQuery($query, array(
            ':login' => array($login, PDO::PARAM_STR)
        ));
        return $this->db->getResults();
    }
}
