<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description de Admin
 *
 * @author thvincent6
 * @version 1.0
 *
 * Un Admin est composée d'un login et d'un role.
 * Elle est constituée de ses Getter et Setter sans restriction.
 */
class Admin {
    private $login;
    private $role;

    public function __construct($login, $role) {
        $this->login = $login;
        $this->$role = $role;
    }

    public function getLogin() {
        return $this->login;
    }

    public function getRole() {
        return $this->role;
    }

    public function setLogin($login): void {
        $this->login = $login;
    }

    public function setRole($role): void {
        $this->role = $role;
    }
}
