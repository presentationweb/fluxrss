<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description de News
 *
 * @author thvincent6
 * @version 1.0
 *
 * Une News est composée d'une url, date, ref, titre, et une desc.
 * Elle est constituée de ses Getter et Setter sans restriction.
 */
class News {
    private $url;
    private $date;
    private $ref;
    private $titre;
    private $desc;

    public function __construct($url, $date, $ref, $titre, $desc) {
        $this->url = $url;
        $this->date = $date;
        $this->ref = $ref;
        $this->titre = $titre;
        $this->desc = $desc;
    }

    public function getUrl() {
        return $this->url;
    }

    public function getDate() {
        return $this->date;
    }

    public function getRef() {
        return $this->ref;
    }

    public function getTitre() {
        return $this->titre;
    }

    public function getDesc() {
        return $this->desc;
    }

    private function setUrl($url) {
        $this->url = $url;
    }

    private function setDate($date) {
        $this->date = $date;
    }

    private function setRef($ref) {
        $this->ref = $ref;
    }

    private function setTitre($titre) {
        $this->titre = $titre;
    }

    private function setDesc($desc) {
        $this->desc = $desc;
    }

    /**
     * Réécriture d'une News sous le format :
     *
     * <Titre> \n <Date> <Description>
     *
     * @return Un string de la News
     */
    public function __toString() : string {
        return $this->getTitre()."<br />".$this->getDate()." ".$this->getDesc();
    }
}
