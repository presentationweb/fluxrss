<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description de NewsGateway
 *
 * @author thvincent6
 * @version 1.0
 *
 * C'est une classe qui permet de faire la pacerelle entre les News et la base de donnée.
 */
class NewsGateway {
    private $db;

    /**
     * Permet d'instancier la classe avec une Connection
     *
     * @param db C'est une instance de Connection qui permet de l'instancier une seule fois
     */
    public function __construct(Connection $db) {
        $this->db=$db;
    }

    /**
     * Permet de chercher en base de donnée toutes les News avec une taille et une page donnée.
     *
     * @param page C'est le numéro de la page à afficher
     * @param taille C'est le nombre de News à afficher
     *
     * @return La liste des News donnée en fonction des paramètres
     */
    public function getNews(int $page, int $taille){
        $query = 'SELECT * FROM News ORDER BY Heure DESC LIMIT :limitePage OFFSET :page';
        $this->db->executeQuery($query , array(
            ':limitePage' => array($taille, PDO::PARAM_INT),
            ':page' => array(($page-1)*$taille, PDO::PARAM_INT)
        ));
        return $this->db->getResults();
    }

    /**
     * Permet de savoir le nombre de page maximal à afficher.
     *
     * @return Un entier qui est le nombre de page maximal.
     */
    public function getMaxPage() :int{
        $query = 'SELECT * FROM News';
        $this->db->executeQuery($query, array());
        $result = $this->db->getResults();
        return count($result);
    }

    /**
     * Permet de rajouter une News en base de donnée
     *
     * @param news C'est la News qui va être ajouté en base de donnée
     */
    public function insert(News $news) {
        $query = 'INSERT INTO News VALUES(:url, :date, :ref, :titre, :desc)';
        $this->db->executeQuery($query, array(
            ':url' => array($news->getUrl(), PDO::PARAM_STR),
            ':date' => array($news->getDate(), PDO::PARAM_STR),
            ':ref' => array($news->getRef(), PDO::PARAM_STR),
            ':titre' => array($news->getTitre(), PDO::PARAM_STR),
            ':desc' => array($news->getDesc(), PDO::PARAM_STR)
        ));
    }

    /**
     * Permet de modifier une News en base de donnée (A coder)
     *
     * @param news C'est la News à modifier
     */
    public function update(News $news) {

    }

    /**
     * Permet de supprimer une News en base de donnée
     *
     * @param url C'est la clé primaire d'une News
     */
    public function delete(string $url) {
        $query = 'DELETE FROM News WHERE URL = :url';
        $this->db->executeQuery($query , array(
            ':url' => array($url, PDO::PARAM_STR)
        ));
    }
}
