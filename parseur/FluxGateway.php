<?php
/**
 * Created by PhpStorm.
 * User: thvincent6
 * Date: 09/01/20
 * Time: 15:59
 */

class FluxGateway{
    private $db;

    public function __construct(Connection $db) {
        $this->db=$db;
    }

    public function getFlux() : ?array{
        $query = 'SELECT URL FROM Flux';
        $this->db->executeQuery($query);
        return $this->db->getResults();
    }
}
