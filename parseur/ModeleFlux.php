<?php
/**
 * Created by PhpStorm.
 * User: thvincent6
 * Date: 10/01/20
 * Time: 11:08
 */

class ModeleFlux{
    private $gw;

    public function __construct(){
        global $dsn, $login, $mdp;
        $this->gw = new FluxGateway(new Connection($dsn, $login, $mdp));
    }

    public function getFlux() :array {
        return $this->gw->getFlux();
    }
}