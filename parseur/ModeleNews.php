<?php
/**
 * Created by PhpStorm.
 * User: thvincent6
 * Date: 10/01/20
 * Time: 11:17
 */

class ModeleNews{
    private $gw;

    public function __construct(){
        global $dsn, $login, $mdp;
        $this->gw = new NewsGateway(new Connection($dsn, $login, $mdp));
    }

    public function supprimerAll() {
        $this->gw->deleteAll();
    }

    public function ajouter(SimpleXMLElement $simpleXMLElement){
        $this->gw->ajouter($simpleXMLElement);
    }
}