<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

class NewsGateway {
    private $db;

    public function __construct(Connection $db) {
        $this->db=$db;
    }

    public function ajouter(SimpleXMLElement $simpleXMLElement) {
        try{
            $query = 'INSERT INTO News VALUES(:url, :heure, :ref, :titre, :desc)';
            $this->db->executeQuery($query, array(
                ':url' => array($simpleXMLElement->link, PDO::PARAM_STR),
                ':heure' => array(date("Y-m-d H:i:s", strtotime($simpleXMLElement->pubDate)), PDO::PARAM_STR),
                ':ref' => array($simpleXMLElement->title, PDO::PARAM_STR),
                ':titre' => array($simpleXMLElement->title, PDO::PARAM_STR),
                ':desc' => array($simpleXMLElement->description, PDO::PARAM_STR)
            ));
        }catch (PDOException $e){

        }
    }

    public function deleteAll() {
        $query = 'DELETE FROM News';
        $this->db->executeQuery($query);
    }
}
