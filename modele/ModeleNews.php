<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description de ModeleNews
 *
 * @author thvincent6
 * @version 1.0
 *
 * C'est une classe qui permet de faire la pacerelle entre les contrôleurs et les metiers.
 */
class ModeleNews {

    private $gw;

    /**
     * Permet d'instancier une nouvelle connection en pré-chargeant
     *
     * Elle intancie une NewsGateway en lui passant également une connection.
     *
     * @see NewsGateway::__construct
     * @see Connection::__construct
     */
    public function __construct(){
        global $dsn, $login, $mdp;
        $this->gw = new NewsGateway(new Connection($dsn, $login, $mdp));
    }

    /**
     * Permet de renvoyer au contrôleur la liste de toutes les news en base avec une taille et une page donnée.
     *
     * @param page C'est le numéro de la page à afficher
     * @param taille C'est le nombre de News à afficher
     *
     * @return La liste des News donnée en fonction des paramètres
     */
    public function getNews(int $page, int $taille) : ? array{
        $results= $this->gw->getNews($page, $taille);
        foreach ($results as $row) {
            $tabNews[] = new News ($row['URL'], $row['Heure'], $row['Reference'], $row['Titre'], $row['Description']);
        }
        if(!isset($tabNews))
            return null;
        return $tabNews;
    }

    /**
     * Permet de savoir le nombre de page maximal à afficher.
     *
     * @return Un entier qui est le nombre de page maximal.
     */
    public function getMaxPage() :int{
        return $this->gw->getMaxPage();
    }
}
