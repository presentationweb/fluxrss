<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description de ModeleAdmin
 *
 * @author thvincent6
 * @version 1.0
 *
 * C'est une classe qui permet de faire la pacerelle entre les contrôleurs et les metiers.
 */
class ModeleAdmin {

    private $gw;
    private $gwNews;
    private $gwFlux;

    /**
     * Permet d'instancier une nouvelle connection et toutes les Gateway utile
     *
     * @see Connection::__construct
     * @see AdminGateway::__construct
     * @see NewsGateway::__construct
     * @see FluxGateway::__construct
     */
    public function __construct(){
        global $dsn, $login, $mdp;
        $this->gw = new AdminGateway(new Connection($dsn, $login, $mdp));
        $this->gwNews = new NewsGateway(new Connection($dsn, $login, $mdp));
        $this->gwFlux = new FluxGateway(new Connection($dsn, $login, $mdp));
    }

    /**
     * Permet de créer la connection administrateur d'un utilisateur.
     *
     * Il utilise la fonctionnalite <i>password_verify</i>
     *
     * @param login C'est le login demandé par l'utilisateur
     * @param password C'est le mot de passe demandé par l'utilisateur
     *
     * @return Un objet soit Null si le login et mot de passe sont incorrects ou une instance de Admin si tout est bon.
     */
    public function connection(string $login, string $password) : ?Admin {
        global $vues, $limitePage;
        $login = Nettoyage::valText($login);
        $password = Nettoyage::valText($password);
        $res = $this->gw->verif_mdp($login);
        if(isset($res)){
            if(!empty($res) && password_verify($password, $res[0]['mdp'])){
                $_SESSION['login']=$login;
                $_SESSION['role']='Admin';
                $_SESSION['taille']=$limitePage;
                return new Admin($login, $password);
            }
            else
                $message="Login ou mot de passe incorrect";
            require($vues['VueErreur']);
            require($vues['VueAuthentification']);
        }
        return null;
    }

    /**
     * Permet verifier si l'utilisateur
     *
     * @return Un objet soit Null si la <b>SESSION</b> est fausse/inexistante ou une instance de Admin si tout est bon.
     */
    static function isAdmin() : ? Admin {
        if(isset($_SESSION['login']) && isset($_SESSION['role']) && isset($_SESSION['taille'])){
            $role = Nettoyage::valText($_SESSION['role']);
            $login = Nettoyage::valText($_SESSION['login']);
            return new Admin($login, $role);
        }
        return null;
    }

    /**
     * Permet de renvoyer au contrôleur la liste de tous les Flux en base avec une taille et une page donnée.
     *
     * @param page C'est le numéro de la page à afficher
     * @param taille C'est le nombre de Flux à afficher
     *
     * @return La liste des Flux donnée en fonction des paramètres
     */
    public function getFlux(int $page, int $taille) : ? array{
        $results = $this->gwFlux->getFlux($page, $taille);
        foreach ($results as $row) {
            $tabFlux[] = new Flux($row['URL'], $row['Titre'], $row['Description']);
        }
        if(!isset($tabFlux))
            return null;
        return $tabFlux;
    }

    /**
     * Permet de savoir le nombre de page maximal à afficher.
     *
     * @return Un entier qui est le nombre de page maximal.
     */
    public function getMaxPageFlux() :int{
        return $this->gwFlux->getMaxPageFlux();
    }

    /**
     * Permet de faire la pacerelle entre le contrôleur et le metier pour ajouter le Flux
     *
     * @param flux Flux à ajouter en base de donnée
     */
    public function ajouterFlux(Flux $flux){
        $this->gwFlux->insert($flux);
    }

    /**
     * Permet de faire la pacerelle entre le contrôleur et le metier pour supprimer un flux
     *
     * @param url L'url du Flux à supprimer (clé primaire)
     */
    public function supprimerFlux(string $url){
        $this->gwFlux->delete($url);
    }

    /**
     * Permet de faire la pacerelle entre le contrôleur et le metier pour ajouter la News
     *
     * @param news News à ajouter en base de donnée
     */
    public function ajouterNews(News $news){
        $this->gwNews->insert($news);
    }

    /**
     * Permet de faire la pacerelle entre le contrôleur et le metier pour supprimer une News
     *
     * @param url L'url de la News à supprimer (clé primaire)
     */
    public function supprimerNews(string $url){
        $this->gwNews->delete($url);
    }
}
