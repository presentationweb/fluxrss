<?php
/**
 * Created by PhpStorm.
 * User: thvincent6
 * Date: 09/01/20
 * Time: 09:36
 */
echo '
    <nav class="navbar navbar-dark bg-dark">
        <a class="navbar-brand" href="?action=">Home</a>
';
        if(isset($vueAdmin)){
            echo '
                <span class="badge badge-danger">Administrateur</span>
            ';
        }
        else{
            echo '
                <span class="badge badge-light">Utilisateur</span>
            ';
        }
echo '
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNav" aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
        </button>
        <div class="collapse navbar-collapse" id="navbarNav">
            <ul class="navbar-nav">
';
                if(isset($vueAdmin)){
                    echo '
                        <li class="nav-item">
                            <a class="nav-link" href="?action=deconnecter">Deconnection </a>
                        </li>
                    ';
                }
                else{
                    echo '
                        <li class="nav-item">
                            <a class="nav-link" href="?action=demandeConnection">Connection </a>
                        </li>
                    ';
                }
echo '
            </ul>
        </div>
    </nav>
';

if(isset($vueAdmin)){
    echo '
        <div class="d-flex justify-content-center">
            <div class="dropdown">
                <button class="btn btn-secondary dropdown-toggle" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                    News
                </button>
                <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
                    <a class="dropdown-item" href="?action=">Afficher</a>
                    <a class="dropdown-item" href="?action=afficherAjouterNews">Ajouter</a>
                    <a class="dropdown-item" href="?action=afficherSupprimerNews">Supprimer</a>
                </div>
            </div>
            <div class="dropdown">
                <button class="btn btn-secondary dropdown-toggle" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                    Flux
                </button>
                <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
                    <a class="dropdown-item" href="?action=afficherFlux">Afficher</a>
                    <a class="dropdown-item" href="?action=afficherAjouterFlux">Ajouter</a>
                    <a class="dropdown-item" href="?action=afficherSupprimerFlux">Supprimer</a>
                </div>
            </div>
            <div class="dropdown">
                <button class="btn btn-secondary dropdown-toggle" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                    Global
                </button>
                <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
                    <a class="dropdown-item" href="?action=parametrePage">Taille de la page</a>
                    <a class="dropdown-item" href="?action=deconnecter">Deconnection</a>
                </div>
            </div>
        </div>
    ';
}

?>

<!--

<div>
    <div class="btn-group" role="group" aria-label="Basic example">
        <span class="badge badge-primary">News</span>
        <a href="?action=" style="color: #FFF"><button type="button" class="btn btn-secondary">Afficher</button></a>
        <a href="?action=afficherAjouterNews" style="color: #FFF"><button type="button" class="btn btn-secondary">Ajouter</button></a>
        <a href="?action=afficherSupprimerNews" style="color: #FFF"><button type="button" class="btn btn-secondary">Supprimer</button></a>
    </div>
    <div class="btn-group" role="group" aria-label="Basic example">
        <span class="badge badge-success">Flux</span>
        <a href="?action=afficherFlux" style="color: #FFF"><button type="button" class="btn btn-secondary">Afficher</button></a>
        <a href="?action=afficherAjouterFlux" style="color: #FFF"><button type="button" class="btn btn-secondary">Ajouter</button></a>
        <a href="?action=afficherSupprimerFlux" style="color: #FFF"><button type="button" class="btn btn-secondary">Supprimer</button></a>
    </div>
    <div class="btn-group" role="group" aria-label="Basic example">
        <span class="badge badge-warning">Global</span>
        <a href="?action=parametrePage" style="color: #FFF"><button type="button" class="btn btn-secondary">Taille de la page</button></a>
        <a href="?action=deconnecter" style="color: #FFF"><button type="button" class="btn btn-secondary">Deconnection</button></a>
    </div>
</div>
-->