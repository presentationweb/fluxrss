<?php
/**
 * Created by PhpStorm.
 * User: thvincent6
 * Date: 09/01/20
 * Time: 16:17
 */

echo '
    <form method="post" action="?action=ajouterFlux">
        <div class="form-group">
            <label for="urlNews">Url du Flux : </label>
            <input type="url" class="form-control" id="urlNews" name="url" placeholder="Ex : www.google.com">
        </div>
        <div class="form-group">
            <label for="titreNews">Titre du Flux : </label>
            <input type="text" class="form-control" id="titreNews" name="titre" >
        </div>
        <div class="form-group">
            <label for="descNews">Description du Flux : </label>
            <input type="text" class="form-control" id="descNews" name="desc" >
        </div>
        <button type="submit" class="btn btn-primary">Valider</button>
    </form>
';


?>
