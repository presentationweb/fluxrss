<?php
/**
 * Created by PhpStorm.
 * User: thvincent6
 * Date: 13/12/19
 * Time: 13:49
 */

echo "<h1>Page d'authentification de l'admin</h1>";

echo '
<form method="post" action="?action=connection">
    <div class="form-group">
        <label for="loginConnection">Login :</label>
        <input type="text" class="form-control" id="loginConnection" name="login">
    </div>
    <div class="form-group">
        <label for="mdpConnection">Mot de passe :</label>
        <input type="password" class="form-control" id="mdpConnection" name="mdp" aria-describedby="aideMDP">
        <small id="aideMDP" class="form-text text-muted">Donnez votre mot de passe à personne !</small>
    </div>
    <button type="submit" class="btn btn-primary">Connection</button>
</form>
';

?>
