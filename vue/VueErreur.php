<?php
/**
 * Created by PhpStorm.
 * User: thvincent6
 * Date: 13/12/19
 * Time: 14:47
 */

if(isset($message)){
    echo '
        <div class="alert alert-danger alert-dismissible fade show" role="alert">
            <strong>Erreur !</strong> '. $message .'
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true">&times;</span>
            </button>
        </div>
    ';
}
else{
    echo '
        <div class="alert alert-danger" role="alert">
            <strong>Erreur !</strong> Un problème est survenu sans en connaître la raison
        </div>
    ';
}


?>