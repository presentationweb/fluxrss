<?php
/**
 * Created by PhpStorm.
 * User: thvincent6
 * Date: 09/01/20
 * Time: 10:42
 */

if(isset($message)){
    echo '
        <div class="alert alert-success alert-dismissible fade show" role="alert">
            '. $message .'
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true">&times;</span>
            </button>
        </div>
    ';
}
else{
    echo '
        <div class="alert alert-succes" role="alert">
            <strong>Succes !</strong> Vous avez réalisé une tâche avec succès 
        </div>
    ';
}


?>