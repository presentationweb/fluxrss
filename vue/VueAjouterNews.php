<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

echo '
    <form method="post" action="?action=ajouterNews">
        <div class="form-group">
            <label for="urlNews">Url de la News : </label>
            <input type="url" class="form-control" id="urlNews" name="url" placeholder="Ex : http://thomas.com">
        </div>
        <div class="form-group">
            <label for="heureNews">Heure de la News : </label>
            <input type="date" class="form-control" id="heureNews" name="heure" >
        </div>
        <div class="form-group">
            <label for="refNews">Référence de la News : </label>
            <input type="text" class="form-control" id="refNews" name="ref" >
        </div>
        <div class="form-group">
            <label for="titreNews">Titre de la News : </label>
            <input type="text" class="form-control" id="titreNews" name="titre" >
        </div>
        <div class="form-group">
            <label for="descNews">Description de la News : </label>
            <input type="text" class="form-control" id="descNews" name="desc" >
        </div>
        <button type="submit" class="btn btn-primary">Valider</button>
    </form>
';

?>
