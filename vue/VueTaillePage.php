<?php
/**
 * Created by PhpStorm.
 * User: thvincent6
 * Date: 09/01/20
 * Time: 11:47
 */
echo '
    <form method="post" action="?action=setPage">
        <div class="d-flex justify-content-center">
            <label for="customRange">Nombre de news par page<br />Minimum : 1 / Maximum : 10 / Pas : 1</label>
        </div>
        <input type="range" class="custom-range" min="1" max="10" step="1" id="customRange" name="taille">
        <div class="d-flex justify-content-center">
            <button type="submit" class="btn btn-primary">Valider</button>
        </div>
    </form>
';
?>