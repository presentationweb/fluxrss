<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
if(isset($tabNews)){
    echo '
        <div class="d-flex justify-content-center">
        <div class="d-flex flex-nowrap">
    ';
    foreach ($tabNews as $news){

        echo '
            <div class="card" style="width: 18rem;">
                <div class="card-body">
                    <h5 class="card-title">'. $news->getTitre() .'</h5>
                    <p class="card-text">['. $news->getDate() . ']: ' . $news->getDesc() .'</p>
                    <a href="'. $news->getUrl() .'" class="btn btn-primary">Visiter le site</a>
                </div>
            </div>
        ';




        //echo '<a href="'. $news->getUrl() .'" ><br />'. $news->getTitre() .'<br />'. $news->getDate() . '<br />' . $news->getDesc() .'</a><br />';
        // $this->getTitre()."<br />".$this->getDate()." ".$this->getDesc()
        
    }
    echo '</div></div>';
}
else{
    echo '
        <div class="alert alert-danger" role="alert">
            <strong>Erreur !</strong> Aucune News dans la base
        </div>
    ';
}

echo '<br />';

echo '<div class="d-flex justify-content-center">';
if(isset($page) && isset($maxPage)) {
    echo '<div class="btn-group" role="group" aria-label="Basic example">';
    if ($page > 1) {
        echo '
        <a href="?action=&page=1" style="color: #FFF"><button type="button" class="btn btn-secondary"> << </button></a>
        <a href="?action=&page=' . ($page - 1) . '" style="color: #FFF"><button type="button" class="btn btn-secondary"> < </button></a>
    ';
    }
    if ($page < $maxPage) {
        echo '
        <a href="?action=&page=' . ($page + 1) . '" style="color: #FFF"><button type="button" class="btn btn-secondary"> > </button></a>
        <a href="?action=&page=' . $maxPage . '" style="color: #FFF"><button type="button" class="btn btn-secondary"> >> </button></a>
    ';
    }
}
else{
    echo '
        <div class="alert alert-danger" role="alert">
            <strong>Erreur !</strong> Problème avec les pages
        </div>
    ';
}
echo '</div>';

echo '</div>';
?>
