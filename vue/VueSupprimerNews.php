<?php
/**
 * Created by PhpStorm.
 * User: thvincent6
 * Date: 06/12/19
 * Time: 15:12
 */
if(isset($tabNews)){

    echo '
        <div class="d-flex justify-content-center">
        <div class="d-flex flex-nowrap">
    ';
    foreach ($tabNews as $news){

        echo '
            <div class="card" style="width: 18rem;">
                <div class="card-body">
                    <h5 class="card-title">'. $news->getTitre() .'</h5>
                    <p class="card-text">['. $news->getDate() . ']: ' . $news->getDesc() .'</p>
                    <div class="d-flex justify-content-center">
                        <a href="'. $news->getUrl() .'" class="btn btn-primary">Visiter le site</a>
                        <a href="?action=supprimerNews&id='. $news->getUrl() .'" class="btn btn-danger">Supprimer le site</a>
                    </div>
                </div>
            </div>
        ';
    }
    echo '</div></div>';
}
else{
    echo '
        <div class="alert alert-danger" role="alert">
            <strong>Erreur !</strong> Aucune News dans la base
        </div>
    ';
}

echo '<br />';
if(isset($page) && isset($maxPage)) {
    echo '<div class="d-flex justify-content-center">';
    echo '<div class="btn-group" role="group" aria-label="Basic example">';
    if ($page > 1) {
        echo '
        <a href="?action=afficherSupprimerNews&page=1" style="color: #FFF"><button type="button" class="btn btn-secondary"> << </button></a>
        <a href="?action=afficherSupprimerNews&page=' . ($page - 1) . '" style="color: #FFF"><button type="button" class="btn btn-secondary"> < </button></a>
    ';
    }
    if ($page < $maxPage) {
        echo '
        <a href="?action=afficherSupprimerNews&page=' . ($page + 1) . '" style="color: #FFF"><button type="button" class="btn btn-secondary"> > </button></a>
        <a href="?action=afficherSupprimerNews&page=' . $maxPage . '" style="color: #FFF"><button type="button" class="btn btn-secondary"> >> </button></a>
    ';
    }
}
else{
    echo '
        <div class="alert alert-danger" role="alert">
            <strong>Erreur !</strong> Problème avec les pages
        </div>
    ';
}

echo '</div>';
echo '</div>';
?>