<?php
/**
 * Created by PhpStorm.
 * User: thvincent6
 * Date: 09/01/20
 * Time: 17:10
 */

if(isset($tabFlux)){
    echo '
        <div class="d-flex justify-content-center">
            <div class="d-flex flex-nowrap">
    ';
    foreach ($tabFlux as $flux){
        echo '
            <div class="card" style="width: 18rem;">
                <div class="card-body">
                    <h5 class="card-title">'. $flux->getTitre() .'</h5>
                    <p class="card-text">'. $flux->getDescription() .'</p>
                    <a href="'. $flux->getUrl() .'" class="btn btn-primary">Observer le flux</a>
                </div>
            </div>
        ';
    }
    echo '</div></div>';
}
else{
    echo '
        <div class="alert alert-danger" role="alert">
            <strong>Erreur !</strong> Aucun Flux dans la base
        </div>
    ';
}

echo '<br />';

if(isset($page) && isset($maxPage)) {
    echo '<div class="btn-group" role="group" aria-label="Basic example">';
    if ($page > 1) {
        echo '
        <a href="?action=afficherFlux&page=1" style="color: #FFF"><button type="button" class="btn btn-secondary"> << </button></a>
        <a href="?action=afficherFlux&page=' . ($page - 1) . '" style="color: #FFF"><button type="button" class="btn btn-secondary"> < </button></a>
    ';
    }
    if ($page < $maxPage) {
        echo '
        <a href="?action=afficherFlux&page=' . ($page + 1) . '" style="color: #FFF"><button type="button" class="btn btn-secondary"> > </button></a>
        <a href="?action=afficherFlux&page=' . $maxPage . '" style="color: #FFF"><button type="button" class="btn btn-secondary"> >> </button></a>
    ';
    }
}
else{
    echo '
        <div class="alert alert-danger" role="alert">
            <strong>Erreur !</strong> Problème avec les pagess
        </div>
    ';

}
echo '</div>';
?>


