-- phpMyAdmin SQL Dump
-- version 4.6.6deb4
-- https://www.phpmyadmin.net/
--
-- Client :  localhost:3306
-- Généré le :  Ven 10 Janvier 2020 à 13:33
-- Version du serveur :  10.1.41-MariaDB-0+deb9u1
-- Version de PHP :  7.0.33-0+deb9u6

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de données :  `dbthvincent6`
--

-- --------------------------------------------------------

--
-- Structure de la table `admin`
--

CREATE TABLE `admin` (
  `login` varchar(50) NOT NULL,
  `mdp` varchar(500) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Contenu de la table `admin`
--

INSERT INTO `admin` (`login`, `mdp`) VALUES
('admin', '$2y$10$humCyCCthA5aQw4gklfnmuf9TR5P8w38H3mMxYo28gkHhqFsRg88u');

-- --------------------------------------------------------

--
-- Structure de la table `Flux`
--

CREATE TABLE `Flux` (
  `URL` varchar(100) NOT NULL,
  `Titre` varchar(200) NOT NULL,
  `Description` varchar(200) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Contenu de la table `Flux`
--

INSERT INTO `Flux` (`URL`, `Titre`, `Description`) VALUES
('http://thomas.com', 'Thomas', 'C\'est mon flux'),
('http://www.jeuxvideo.com/rss/rss.xml', 'Jeux Vidéo', 'C\'est le site de jeux vidéos');

-- --------------------------------------------------------

--
-- Structure de la table `News`
--

CREATE TABLE `News` (
  `URL` varchar(150) NOT NULL,
  `Heure` date NOT NULL,
  `Reference` varchar(200) NOT NULL,
  `Titre` varchar(50) NOT NULL,
  `Description` varchar(500) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Contenu de la table `News`
--

INSERT INTO `News` (`URL`, `Heure`, `Reference`, `Titre`, `Description`) VALUES
('http://google.com', '2020-06-30', 'Y en a tkt', 'Meilleur titre', 'Une autre description'),
('http://simeon.com', '2015-04-01', 'Aucune', 'Une autre news', 'Ouai la description'),
('http://thomas.com', '2020-01-01', 'Nouvel an', 'Bonne année', 'Vive la nouvelle année'),
('http://thomasBG.com', '2020-07-11', 'Thomas', 'Mon anniversaire', 'Ca va être trop bien');

--
-- Index pour les tables exportées
--

--
-- Index pour la table `Flux`
--
ALTER TABLE `Flux`
  ADD PRIMARY KEY (`URL`);

--
-- Index pour la table `News`
--
ALTER TABLE `News`
  ADD PRIMARY KEY (`URL`);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
