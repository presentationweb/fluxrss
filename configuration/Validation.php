<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Validation
 *
 * @author thvincent6
 */
class Validation {
    // aller dans la doc
    public static function valURL(string $chaine) : bool{
        return filter_var($chaine, FILTER_VALIDATE_EMAIL);
    }
    // Comment faire un nettoyage de l'heure ?
    public static function valHEURE(string $chaine) : bool{
        return filter_var($chaine, FILTER_VALIDATE_INT);
    }
    
    public static function valPAGE(string $chaine) : bool{
        return filter_var($chaine, FILTER_VALIDATE_INT);
    }
}
