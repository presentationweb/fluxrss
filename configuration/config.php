<?php

//gen
$rep=__DIR__.'/../';

// liste des modules à inclure

//$dConfig['includes']= array('controleur/Validation.php');

//BD

$base="dbthvincent6";
$login="root";
$mdp="";

//Vues

$vues['Vue']='vue/Vue.php';
$vues['VueNews']='vue/VueNews.php';
$vues['VueFlux']='vue/VueFlux.php';
$vues['VueAjouterNews']='vue/VueAjouterNews.php';
$vues['VueSupprimerNews']='vue/VueSupprimerNews.php';
$vues['VueAuthentification']='vue/VueAuthentification.php';
$vues['VueErreur']='vue/VueErreur.php';
$vues['VueSucces']='vue/VueSucces.php';
$vues['VueTaillePage']='vue/VueTaillePage.php';
$vues['VueAjouterFlux']='vue/VueAjouterFlux.php';
$vues['VueSupprimerFlux']='vue/VueSupprimerFlux.php';

$erreurs['setLoginMdp']="Vous avez pas renseigné le login ou le mot de passe, veuillez réessayer !";
$erreurs['mdpFalse']="Login ou mot de passe incorrect, merci de bien vouloir recommencer !";

// Nombre magique

$limitePage=3;

?>