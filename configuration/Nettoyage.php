<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Nettoyage
 *
 * @author thvincent6
 */
class Nettoyage {
    public static function valText(string $chaine) : string{
        return filter_var($chaine, FILTER_SANITIZE_STRING);
    }

    public static function valUrl(string $chaine) : string{
        return filter_var($chaine, FILTER_SANITIZE_URL);
    }

    public static function valInt(int $chaine) : int{
        return filter_var($chaine, FILTER_SANITIZE_NUMBER_INT);
    }

    // Comment filtrer une heure
}
