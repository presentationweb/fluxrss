<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Connection
 *
 * @author thvincent6
 */
class Connection extends PDO {
    
    private $stmt;
    
    public function __construct(string $dsn, string $username, string $passwd) {
        parent::__construct($dsn, $username, $passwd);
        $this->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
    }
    
    public function executeQuery(string $query, array $parameters = []) : bool{ 
        $this->stmt = parent::prepare($query); 
	foreach ($parameters as $name => $value) { 
            $this->stmt->bindValue($name, $value[0], $value[1]); 
	} 

	return $this->stmt->execute(); 
    }

    public function getResults() : array {
        return $this->stmt->fetchall();
    }
    
}