<?php
/**
 * Created by PhpStorm.
 * User: thvincent6
 * Date: 09/01/20
 * Time: 09:53
 */

require_once('Autoload.php');
require_once('config.php');

session_start();
Autoload::charger();

global $login, $mdp, $base, $vues;
$dsn='mysql:host=localhost;dbname=' . $base;
try{
    new Connection($dsn, $login, $mdp);
} catch (PDOException $e) {
    echo '<br/>Erreur<br/>';
}

if(isset($_SESSION['login']) && isset($_SESSION['role'])){
    if($_SESSION['role'] == 'Admin'){
        $vueAdmin = true;
    }
}
require($vues['Vue']);

new ControleurPrincipal();

?>